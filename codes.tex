\chapter[Codes of Belief]{Codes}
\label{gods_codes}
\index{Gods}
\index{Codes of Belief}

\begin{multicols}{2}
\noindent
Players can receive additional \gls{xp} for following their beliefs.
While anyone is free to give offerings to any of the gods, most people have a primary god they worship, suggested by their birth but decided in adulthood based on shared values.
Others follow no god but have a code of some type, guiding their actions.
These codes are not formal beliefs, written as law and discussed at meetings but rather a set of aspirations which some have.

\iftoggle{verbose}{
  Each god has a holy day marking its favourite time of year.
  During the holy day, anyone can earn \gls{xp} by following the edicts of the god, even those who follow others.
  The day of \gls{wargod} is a day to remember war and settle disputes by fist or steel, the day of \gls{joygod} is one of joy, to be celebrated with pranks and presents.

  The gods are most popular with humans and gnolls. Most dwarven settlements have a temple of some kind but it is not something all dwarves take much interest in except during odd times when they want to pay for a blessing. Gnomes' interactions with the gods mainly consists in chronicling legends about them and debating the nature of divinity, but not actively worshipping them. Elves, it is said, do not have the humility to worship anything.

  The gods presented here are the most important -- they are the ones featured in the larger tales and who have the most prominent holy days. There are, however, many more. Each region or individual tribe has its own little god. Players are encouraged to create their own.

  Each god has a holy day marking its favourite time of year.
  During the holy day, anyone can earn \gls{xp} by following the edicts of the god, even those who follow others.
  The day of \gls{wargod} is a day to remember war and settle disputes by fist or steel, the day of \gls{joygod} is one of joy, to be celebrated with pranks and presents.

  Those without a dedicated deity often dedicate themselves to some informal code instead.
  The codes might be thought of as attitudes or philosophies for life.
  Followers of similar codes may well get along together but they will not recognise each other as members of a similar organisation.
  Those with a code as their primary motivator may also sacrifice to gods or even occasionally worship and donate to temples, but their ultimate aims lie with themselves.
  It is said those who do not fully dedicate themselves to any god must wander the afterlife without aid or guidance -- such spirits always provide the most bizarre and contradictory accounts of death and can prove difficult to summon with Necromancy.
}{}

Those with a personal code can never walk the Path of Divinity.

\end{multicols}

\section{Rulings}

\begin{multicols}{2}

The \gls{gm} decides how much \gls{xp} to give out for any given task -- each path has a number of suggestions but the list should be understood as open-ended and entirely at the whim of the \gls{gm}.

Players can only gain each reward once per session, and only for the greatest reward of any type,
\iftoggle{verbose}{%
so a follower of \gls{deathgod} can receive 5 \glspl{xp} for  \glspl{hp} only once per session, and would not also gain 1  \gls{xp} for losing a single \gls{hp}.

Some codes give a reward for donating or gaining gold.
Only the highest reward counts, so someone cannot gain 1\gls{xp} for donating a gold piece to a temple, and then gain 10 more for donating 100 \glspl{gp} -- the highest sum takes precedence.
}{%
whether that means losing \glspl{hp} or donating \glspl{gp} to a temple.
}

Players receive \glspl{xp} only at the end of a session, and only for achievements they remember -- the \gls{gm} bears no responsibility for note-taking.

\end{multicols}

\section{The Codes}

\begin{multicols}{2}

\iftoggle{verbose}{}{
  \boxPair{
    \acquisitionXP
  }{
    \joyXP
  }
}

\subsection{The Code of Acquisition}
\index{Code!Acquisition}

\iftoggle{verbose}{
    \acquisitionXP
}{}

The goal of life is acquisition.
We all want things, therefore people who get more things are doing better.
Those on the code of acquisition are often those who can acquire more money -- townmasters, dwarves in love with gold, or gnomes who have dedicated their lives to finding the best rubies.

Underneath the exterior love of wealth, those on the Code of Acquisition primarily desire \emph{respect}.
They have a deep sense of needing to be important in the eyes of others, and find their acquisitions the most efficient way to achieving that goal.


\subsection[\Glsentrytext{joygod} -- Goddess of Joy]{\Glsentrytext{joygod}}
\index{Gods!\Glsentrytext{joygod}}

\iftoggle{verbose}{
    \joyXP
}{}

\noindent The goddess of joy delights in pranks and fun of all kinds. Her holy day is in the third season of the first cycle -- a cold time when people are in need of cheering up from the cold winds, when her followers stuff snow down people's back or balance ice-plates on the tops of doors to watch them fall on friends' heads. An eclipse marks the actual day every three cycles.

Her temples are always full of home-brewed beer served by attractive men and women. Often such temples replace regular taverns.

\subsubsection{Spheres}

\noindent Priests of \Glsentrytext{joygod} have access to the illusion and Polymorph spheres.
Their spells appear with a flash of rainbow colours, often accompanied by light, strange sounds similar to a harpsichord.

\subsubsection{Mana Stones}

\Glsentrytext{joygod}'s magical items can be anything which is a simulacrum of anything else -- a toy dagger, a doll, a statue or a painting are all possible mana stones.
Their mana stone spells are activated by a command word.

\iftoggle{verbose}{}{
  \boxPair{
    \knowledgeXP
  }{
    \experienceXP
  }
}

\subsection{\Glsentrytext{knowledgegod} -- God of Illumination}
\index{Gods!\Glsentrytext{knowledgegod}}

\iftoggle{verbose}{
  \knowledgeXP
}{}

\noindent The god of light is popular among all the land, especially with scholars, as he is a god of knowledge.

Followers of the god of light have access to the illusion and Force spheres.
His mana stones always contain the writings of famous works -- usually from the Holy Book of Light but potentially from any learned source.
The item in question must be at least as large as a sheet of paper -- commonly a book, potentially an armoured breast-plate but never a sword or rock.
His spells appear in a warm glow of light, illuminating an area with a glow the strength of a few candles brighter than the ambient lighting.
The mana stones of \Gls{knowledgegod} are always activated by a command word.


\subsection{The Code of Experience}
\index{Code!Experience}

\iftoggle{verbose}{
  \experienceXP
}{}

The world is here to be lived, to be known, to be connected with.
You want all the experiences -- unique experiences, sacred experiences, horrible experiences; the only real evil is boring repetition.
You want to stare at the full moon while drinking with friends, to create some new piece of art and to feel enough heart-ache to make you physically sick.
Elation and deep-rooted fear are equally valuable -- they are both life.
You also value giving life and meaning to the old and abandoned, to experiencing what few others have experienced, whether it's finding a lost and neglected poem from an old language or visiting an area never before seen by people.

Those with the Code of Experience tend to explore, to lead troupes into new areas, to try different approaches, and test new solutions to entrenched problems.
They tend to create, but this does not necessarily require the Crafts or Performance skills -- someone could equally design some new use for magic, or perfect a new type of knot.

\subsection{\Glsentrytext{naturegod} -- Goddess of the Forest}
\index{Gods!\Glsentrytext{naturegod}}

\iftoggle{verbose}{
  \natureXP
}{}

\noindent
\Gls{naturegod} is the mother of all the growing green plants and all the animals.
Farmers worship her as they know their produce ultimately stem from the forest.
Her holy day is a feast-day during the warm first season of the third cycle.
She has few temples but many followers.
Those temples are usually arranged around some particularly striking tree, often magically altered to appear fantastically beautiful or just warped.
Farmers are fond of putting up a little shrine to her with no more than a few rocks and a unique tree, and sometimes with a bird feeder.
Her followers are numerous -- they meet during feast days, especially \gls{naturegod}'s own day of feasting.
On other days, they simply travel, and expect \gls{naturegod}'s blessings and the good will of the people around them to provide food for them, occasionally giving out her blessings if they have been initiated into the secrets of her divine powers.

Those casting spells on her Path of Divinity find things appearing in a wave of mist while flowers bloom nearby.
They are granted access to the Polymorph and conjuration spheres.
The mana stones of her followers are always animals or plants.
If the animal in question has access to a spell, the animal as well as the priest always has the ability to cast spells.
Her followers commonly have large dog companions which are able to give blessings or summon other dogs for help with the conjuration sphere.
Plants with a spell are always activated by a command word.
Animals with a spell implanted always activate the spell at their own behest and rarely at the right time; cats have been known to use implanted spells to hunt prey while a dog which feels threatened might reflexively turn into a rat when scared.

\iftoggle{verbose}{}{
  \boxPair{
    \natureXP
  }{
    \warXP
  }
}

\iftoggle{verbose}{
  \warXP
}{}

\subsection{\Glsentrytext{wargod} -- Goddess of War}
\index{Gods!\Glsentrytext{wargod}}

\noindent \Gls{deathgod}'s big sister, \gls{wargod}, is a mighty warrior.
To be worthy of her, people must train well and be fast in battle.
Her temples are few and are often no more than small rooms within a larger barracks, but her priests travel on almost every martial campaign -- even those who follow other gods usually object to going into battle without the blessings of a cleric of \gls{wargod}.

\gls{wargod}'s feast day ends the fourth and last season of the third and last cycle.
On this day, if no battles are present, entire towns sometimes gather together to voice their frustrations, calling each other out to one-on-one fights.
There is no reprisal for the result of these fights -- they stand alone, and no redress can be made in a socially acceptable way until \gls{wargod}'s next holy day.

\subsubsection{Spheres}

Clerics of \Gls{wargod} have access to the Invocation and Conjuration spheres.
They enjoy summoning weapons, hordes of helpers and raining down divine wrath in the form of fire and lightning upon their opponents.
Their spells are accompanied by loud, terrifying noises which can be heard for up to a mile around and shining, silvery flashes from where fire and battle cries appear.

\subsubsection{Mana Stones}

Their mana stones are weapons or hunting trophies.
Weapons can only store 2 MP per point of Damage they inflict.
Hunting trophies can hold up to 1 MP for every \glspl{hp} of the beast killed.

\iftoggle{verbose}{}{
  \boxPair{
    \deathXP
  }{
    \tribeXP
  }
}

\iftoggle{verbose}{
  \deathXP
}{}

\subsection{\Glsentrytext{deathgod} -- God of the Grave}
\index{Gods!\Glsentrytext{deathgod}}

\noindent \Gls{wargod}'s less popular little brother rules over death and the suffering which precedes it.
He teaches us to remember our own dead fondly and to desecrate the graves of our enemies so that they can be forgotten.
His feast day is during the great storms of the first season of the first cycle.
Volcanoes often explode to mark this occasion.
His temples are few and far between -- a couple of large cities with important people buried, the occasional gnoll hut where a mad shaman of death collects skulls and speaks strange promises about a coming war or a deep, dwarven catacomb where the honoured dead of many a dwarf want to gain the promise of being lead to the halls of the honoured dead.


\subsubsection{Spheres}

Clerics of \gls{deathgod} have access to the Necromancy and Enchantment spheres.
They employ magic to trap or terrify people while raising corpses from the grave to fight.
Their spells arise in a pool of inky blackness and are accompanied by the foul smell of old, rotting meat.

\subsubsection{Mana Stones}

\Gls{deathgod}'s mana stones are always made from the glorious dead.
Mana stone can hold half the \gls{fp} of the original target (rounded up).
The hand of a man who had 6 \glspl{fp} could store up to 3 \gls{mp}.

Spells implanted in those mana stones are always activated by a command word.

\subsection{The Code of the Tribe}
\index{Code!Tribe}

\iftoggle{verbose}{
  \tribeXP
}{}

What's important is you and yours.
Your children, the memory of your grandparents, the honour of the tribe.
Your children will be your legacy, while you must die your legacy can live on forever.
If you want to do well in this world, you have to put you and yours first.
This path is popular among gnolls, humans and dwarves, who can become very family-focussed.
Exactly who counts as being `in the tribe' does not have to be limited to blood relatives, however -- it's an intuitive thing.
You know your own.

Travelling companions do not automatically count as members of your tribe, but they may come to in time. Exactly what counts as a `tribe' is mostly in the hands of a player, though the bonds should never be made lightly.

\subsection{\Glsentrytext{justicegod} -- God of Justice}
\index{Gods!\Glsentrytext{justicegod}}

\iftoggle{verbose}{
  \justiceXP
}{}

\noindent Warden to all oaths, lord of ten thousand holy warriors, leader of armies, the giver of vengeance and punishments -- \gls{justicegod} is a popular god.
He is invoked during wedding vows and business deals.
His followers are found among the politically influential and can be some of the most zealous of religious followers.
He values obeying the law, making fair deals, being a good host and supporting the poor.

His holy day is during the second season of the second cycle.
It is considered extremely good faith to make an oath on this day, and mortally bad luck to break such an oath.

\subsubsection{Spheres}

\noindent \Glsentrytext{justicegod}'s clerics can access the enchantment and Force spheres.
They use enchantment to gain followers, dazzling them with the glory of the purity and strength of their god, while force is used to protect the innocent and faithful.
Their spells appear in a shimmer of gold and the sound of a gong.

\subsubsection{Mana Stones}

\Gls{justicegod}'s mana stones are always people who are followers of \Gls{justicegod}.
Those believers alone can activate any spells which are stored inside them.
Priests of \Gls{justicegod} often gift their followers with single-use magical powers, such as the ability to call upon a blessing or the ability to protect themselves with armour.
If the people who are being used as mana stones are given spells then they can activate those spells at will with a short prayer by spending 4 \glspl{ap}.

\end{multicols}
