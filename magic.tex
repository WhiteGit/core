% these lines initiatiate the 'magic' mini-table of contents, so we can restart it later

\chapter{Magic}
\label{magic_paths}
\index{Magic!Paths of Magic}

\iftoggle{verbose}{%

\begin{figure*}[b!]

\begin{boxtable}[lp{.25\textwidth}X]

  Path & Spheres & Flavour \\\hline

  Alchemy & Conjuration, Invocation, Force, Illusion, Necromancy & Alchemists use sacred geometry and the power of precious metals and minerals to twist the world around them. \\

  Blood & Aldaron, Enchantment, Force, Invocation, Polymorph & Creatures with innate magic simply call to the world to change the weather, change targets' species, and move items with the power of their minds.  It is used by elves, dragons, and sorcerers with elven blood. \\

  Devotion (\glsentrytext{naturegod}) & Aldaron, Conjuration, Fate, Polymorph & \glsentrytext{naturegod} blesses rare priests of the forest with the ability to change local weather conditions, and cast divine light. \\

  Devotion (\glsentrytext{justicegod}) & Aldaron, Enchantment, Fate, Force & Followers of \gls{justicegod} channel their god to protect the innocent and righteous with blessings and raw magical force.  Evil creatures can be detected, then be ordered to stop, turn and flee. \\

  Runes & Conjuration, Fate, Force, Illusion, Necromancy & Rune magics take a long time as the spells must literally be painted or carved into items. The resulting spells are often placed into items for a quick release, or cast ahead of time. Rune magics are powerful but require craft and preparation. \\

  Song & Aldaron, Enchantment, Fate, Illusion & Song magic must be cast slowly, as the spells are literally songs. The spells have subtle effects but song magic is no less powerful than other spheres. \\

\end{boxtable}

\end{figure*}

\begin{multicols}{2}

\noindent
\Glspl{miracleworker} create spells by taking a basic spell, and adding \textit{Enhancements}.
A young elf might begin being able to cast the \textit{Mist} spell.
She focuses for a moment, and a small patch of mist forms where she looks.
She can cast it a few times before feeling tired, but if she pushes it any further she'll end up feeling sick for weeks.

After some years of practice, she learns to modify her spells in a few ways.
She can make \textit{Colourful Mist} appear in red, green or blue, or cast a \textit{Wide Mist} over a small area, obscuring the view in half a room.
She can also cast a \textit{Fast Mist}, which appears instantly.

As she explores other types of magic, all her spells get better as she understands the wider patterns which link them.
She can now cast a \textit{Subtle Mist} without moving her fingers or even showing much stress in her eyes, so nobody can tell where the mist comes from.
The spell still has limited use.
She doesn't pull it out much in dangerous situations.

Later in life, with diligent practice, she will learn more \textit{Aldaron} magic, improving the mist, and learn how to add more to her spells.
She'll be able to cast a \textit{Wide, Colourful Mist} which looks a little like a giant, red phoenix, or the mist could be \textit{Fast} and \textit{Wide}, instantly filling a room.

As \glspl{miracleworker} progress, they bind together grander, more complicated spells.

\end{multicols}

}{}

\section{The Paths of Magic}

\begin{multicols}{2}

\iftoggle{verbose}{
  \noindent
  There are various roads to learning magic -- each allows the mage to invoke different spheres and has a different flavour of magic.
  Acquiring a new Path only requires purchasing the appropriate Knack.

  Each school of magic has its own flavour but different people casting spells from the same spheres of magic will end up with exactly the same results, mechanically.
  A priest of war may call divine fire to to destroy enemies where an alchemist uses precise gestures to summon the essential form of fire, but both are just using the Invocation sphere.
}{}

\subsection{Multiple Paths}

People can pick up different Paths of Magic by purchasing multiple Knacks.
If someone begins as a Blood mage with Aldaron 2 then later learns Song Magic, they would be able to cast Aldaron through Song magic with only a little practice.
In gaming terms, the first \gls{downtime} grants full access to all spheres from any secondary Path.

However, the opposite is not true.
Those on the Path of Song learn spheres easier than others, spending 5 fewer \glspl{xp} to purchase a sphere.
If a sphere has not been `fully' paid for, then the remaining \glspl{xp} must be spent to use that sphere on a different path.

\end{multicols}

\section[Spellcasting]{Casting Your First Spell}
\label{basiccasting}

\begin{multicols}{2}

\subsection{Casting}

Spells are cast by spending a number of \gls{mp} equal to the spell's level, so 1st level spells always cost 1 \gls{mp} and 3rd level spells always cost 3 \gls{mp}.
The character then spends the mana and makes a roll against some \gls{tn} to cast the spell.

\index{Mana}\subsection{Mana}

Everyone's total mana is equal to triple the number of spheres they have, plus their Wits Bonus.
\iftoggle{verbose}{%
  For example, a caster with Invocation 3, Fate 1, and a Wits Bonus of +2, would have 8 \glspl{mp} in total.
}{}

If a caster has no \gls{mp} left, they can still cast spells by paying the cost with \gls{hp} instead of \gls{mp}.
\iftoggle{verbose}{%
The magical energies pull the power they need from the blood and bones of the caster, leaving them with a bleeding nose, raging headache and sometimes stranger effects such as acidic pustules or discoloured skin patches.
Many a desperate caster has died through the use of their own magic rather than an enemy's sword; a wizard with their back to the wall is a dangerous opponent indeed.
}{}
\Glspl{fp} cannot be spent in lieu of \glspl{hp}.

Mana is a fickle thing -- when lazing \ around a village it can take hours to regain even a little driblet of magic.
When fighting in deep caves, a few minutes' focus can summon most of a mage's magical energies back.
Every scene, characters regenerate 2 Mana plus their Wits Bonus.
If this total would be 0 then the amount of time required to gather a single \gls{mp} increases by 1 scene.
Characters with a Wits score of -2 must wait 2 scenes before regenerating 1 \gls{mp} while those with Wits -3 must wait 3 scenes.

\subsection{Range}

\sidebox{

\begin{boxtable}[XX]

  Wits & Range \\\hline

  -2 & 3 \\

  -1 & 4 \\

  0 & 5 \\

  1 & 10 \\

  2 & 15 \\

  3 & 20 \\

\end{boxtable}

}


Spells have a range of 5 squares plus 5 times the caster's Wits bonus.
A negative Wits Bonus decreases the range by one square per penalty.

\iftoggle{verbose}{
Magic extends all around the character but mages can rarely affect targets anywhere near the range of a good archer.
}{}

Spells which affect a large area are only restricted by where they \emph{start}.
A \textit{Wide Fireball} covering 3 squares might be cast 5 squares away, but it could extend past that, reaching a total of 7 squares.

\iftoggle{verbose}{%

This range limitation applies to all magic, including Song magic.
While a tune may carry over the hilltops, the force of the magic usually remains close to the caster.

}{}

If a caster leaves an active spell, the spell remains, with no regard for range limitations.

\iftoggle{verbose}{%
  A mage could cast an illusion over a dog, and leave the area, but the spell remains despite the dog being miles away.
}{}

\subsection{Instant \& Continuous Spells}
\index{Continuous Spells}
\index{Instant Spells}

Some spells are instant -- a ball of fire flashes from the mage and incinerates someone, or a touch grants the favour of the gods, healing FP -- but most are `\textit{continuous}'.
Continuous spells can be cancelled at will or maintained indefinitely.
However, while they are being maintained, the \gls{mp} required to cast them remains spent, lowering the mage's maximum \gls{mp}.%
\iftoggle{verbose}{%
\footnote{Spell Enhancements (see below) do not lock mana in this way.}

For example, Tauron the elven sorcerer casts a spell on himself to appear as a human -- all the better to blend into surrounding society.
He spends 1 \gls{mp}.
Later, he enchants an animal to be his companion for 2 \gls{mp}.
Normally, his maximum \gls{mp} is 6, but he is currently reduced to a maximum of 3 \gls{mp} so long as he continues to be a bear-riding man.
}{}

These still-active spells are known as \glspl{standingspell}.
Some mages operate by continuously casting different spells and then going `empty' when the mana is gone.
Others typically operate with \gls{standingspell} alone, casting everything they might need before the day begins and leaving their useful spells `running' but leaving themselves unable to cast more.

\subsection{Casting Times}

\iftoggle{verbose}{
  Your standard spell takes a while to cast -- specifically a number of rounds equal to half the spell's level.
  A first level spell takes a full \gls{round} to cast, and activates at the end of a round, while a third level spell requires 2 \glspl{round} to cast.
  When casting spells in combat, \pgls{miracleworker} will generally use the \textit{Fast} Enhancement (page \pageref{fast}).
}{
  Spells require a number of full \glspl{round} to cast equal to half the spell's level.
}

Casters who take any other action during this time, such as evading an attack or speaking, lose the spell and must start over (but lose no \glspl{mp}).

\subsubsection{Ritual Spells}

Mages who take their time over spells can attempt a Ritual Spell -- they cast it as a \gls{restingaction}%
\footnote{See page \pageref{restingactions}.}.
The mage can gain mana slowly, spending some, drawing more from a mana stone or item, then spending more before finally casting the spell.

Ritual spells can also be cast as a team effort -- any number of spell casters who are on the same \gls{path} of magic can cast any spell they all know together.
They can each invest \gls{mp} to create \gls{standingspell}, and thereafter any one of them can cancel the spell.
\iftoggle{verbose}{%
  As usual, the primary caster can spend mana up to double their Metamagic score (meaning total \glspl{mp}), the second caster can add a maximum number of \glspl{mp} up to their regular total, and the next caster can \glspl{mp} up to a quarter of their total, and so on.
}{}

\subsection{Spell Enhancements}
\label{Enhancements}

Spell Enhancements make spells stronger, at the expense of adding to the spell's level, and \gls{tn}.
These Enhancements always take the form of adjectives.

\iftoggle{verbose}{
  The \textit{Lending Hand} spell allows someone to add a Bonus to an ally's rolls at the cost of 1 \gls{mp}.
  But anyone with the \textit{Wide} Enhancement can add a level, and cast a \textit{Wide Lending Hand} spell, which boosts multiple allies' rolls as a second level spell (costing 2 \glspl{mp}; or with the \textit{Fast} Enhancement, \pgls{miracleworker} could cast a \textit{Fast Lending Hand} spell.

  The Invocation sphere in particular may seem very limited, where casters can only benefit from a Level 1 \textit{Fireball}.
  However, anyone with Invocation 3 could spend 3 \glspl{mp} to cast a \textit{Fast, Potent, Fireball}, or a \textit{Fast, Wide, Fireball}, which targets many enemies instantly.

  \Glspl{miracleworker} gain access to Enhancements by raising their Metamagic level (which is always equal to their maximum \glspl{mp}).

  \subsection{Spell Failure}
  Spells are always cast, even if the caster does not get the intended result.
  If an illusion spell fails, the illusion persists -- it simply looks obviously magical, like bad CGI.
  If a fireball spell fails, fire still comes, as the mage's mana has to go \emph{somewhere}, even if nothing hits the intended target.
}{
  Casters gain Enhancements through gaining Metamagic levels.
}

\end{multicols}

\sphere{Metamagic}
\index{Metamagic}

\begin{multicols}{2}

\index{Path Rating}

\noindent
The more \glsentrylongpl{mp} a mage has, the more Metamagic they have to change their spells.
A mage with 2 spheres and an Intelligence Bonus of +1 would have a total Metamagic rating of 7.

Every power level offers the caster new spells or Enhancements (which make spells more powerful).
The Enhancements drawn from Metamagic can affect all spells the caster has, making every one more powerful.
In this way, spells can be combined for greater effects.

Anyone who has bought any magical Knacks can make use of any of these abilities, even if they haven't bought any spheres.

\spelllevel

\spell{Identify Item}{Instant}{Academics}{Find out if an item is magical}\\
The mage detects whether or not something is a mana stone (i.e. an item or person which stores mana).

\spell{Mana Stones}{Continuous}{Academics}{Create a vessel for \glspl{mp} equal to double the \glspl{mp} you sacrifice}

A mana stone is an item which stores mana, and each path of magic has its own version.\footnote{See page \pageref{magic_paths} for more on Paths of Magic.}
Once an item (or creature) is designated as a mana stone, the item becomes a vessel which can store up to 2 \glspl{mp} per level of the spell.

For example, a mage with 5 \glspl{mp} might cast this spell at level 3.
The mage would be left with only 2 \glspl{mp} to use, but the stone would have 6 \glspl{mp}.

Anyone on the same Path of Magic can retrieve the mana from the stone by simply touching it and concentrating.
The spell is always permanent -- no additional mana must be kept aside so that the spell remains active.
Retrieving the mana takes the normal amount of time to use an item -- 4 \glspl{ap}.

The mana in mana stones cannot be used to create more mana stones and mages cannot enter their own temporary \gls{mp} into the mana stone.

Mana stones form the basis of all magical items, and \glspl{miracleworker} can only use their traditional mana stones to create magical items.

\subsubsection{Ambient Mana Regeneration}
\label{ambientmana}
\index{Ambient Mana}
These stones always start life empty, but regenerate \glspl{mp} each scene until they reach their maximum.
Mana stones only fill up through the ambient mana in the air.
Typically, this means 2 \glspl{mp} at the end of each scene, or 3 within a deep forest, or anywhere secluded.

However, there is only so much mana to go around.
Multiple mana stones in an area must divide the mana between them.
Whichever has the most empty slots restores mana first, so if one item has 1 out of 5 \glspl{mp} left, while another has 8 out of a maximum 9 \glspl{mp} left, the first item takes ambient \glspl{mp} first, because it has 4 empty points, so it draws more in.
If items are tied, roll a die to see which regenerates \glspl{mp} first.

\iftoggle{verbose}{
  For this reason, parties on some \gls{adventure} cannot make use of dozens of magical items at once.
}{}

\spell{Imbuing}{Instant}{Empathy}{Add \glspl{mp} to a mana stone}

The mage spills a number of \glsentrylongpl{mp} into a mana stone created through the same path of magic the mage walks.
The maximum number recharged in a single spell is equal to the caster's Intelligence Bonus.
If cast as a \textit{Wide} spell, the mage can spend \glspl{mp} to spread between multiple items, but can only Imbue total \glspl{mp} equal to what is spent.

\spelllevel

\spell{Identify Mana}{Instant}{Empathy}{Find out which path of magic made an item}
\label{detectmagic}

The caster identifies which Path of magic someone is walking, or which Path was used to create an item which holds \glsentrylongpl{mp}.

\spell{Detect Mana}{Instant}{Empathy}{Find out exactly how many \glspl{mp} are in the target}

The mage casts the spell on any person or item and finds out how many \glsentrylongpl{mp} the target has, including any mana stones the target has.

\spelllevel

\enhancement{1}{Fast}{Make a spell cast instantly}\label{fast}
\iftoggle{verbose}{%
This Enhancement alone allows \glspl{miracleworker} to  cast effectively on the battlefield.
With this Enhancement, a spell no longer requires one round per level to cast, but can be cast with 1 \gls{ap}, plus the spell's level, minus the caster's Wits Bonus (minimum of 1).

While a standard \textit{Oath} spell requires 2 \glspl{round} to cast, a \textit{Fast Oath}, cast at level 3, requires only 4 \glspl{ap} to cast.
}{
\textit{Fast} spells can be cast instantly, and require only 1 \gls{ap}, plus the spell's level, minus the caster's Wits Bonus.
}

\spelllevel

\enhancement{1}{Colourful}{Change a spell's appearance}

This Enhancement allows \pgls{miracleworker} to alter the appearance of any spell.
The changes are purely cosmetic.
\iftoggle{verbose}{%
  This can let a \textit{fireball} take the shape of a bird, or add colour to shadows, turning them into complete illusions.
}{}

\enhancement{1}{Wide}{Widen the spell to hit \glsentrylong{lv} + Wits targets}
\label{wide_enhancement}
The spell extends to cover a wide area -- a total area equal to the spell's level, plus the caster's Wits Bonus.
The squares are always continuous, so a spell targeting four squares could form a $2\times 2$ area, or four continuous squares.

If the spell targets people, one person per square is always a reasonable baseline, but more might be targeted in a narrow tunnel, or fewer if targets are spread out to surround the party.

\spelllevel

\enhancement{1}{Potent}{Increase the caster's effective Intelligence Bonus by +1}
\label{potent}

A \textit{Potent} spell casts as if the mage had +1 Intelligence.
\iftoggle{verbose}{%
A \textit{Potent, Colourful, Cloak} Illusion spell would be harder to spot and a \textit{Potent Mage Armour} would gain the benefits of both the additional level, and the Intelligence Bonus.

A mage with +1 Intelligence could cast a \textit{Potent Pocket Spell} which activates with a +2 Bonus, and lasts for 2 scenes, and the same applies to Talismans and Artefacts.

Making a resisted spell \textit{Potent} usually has no effect, because the Enhancement will raise the spell's \gls{tn}.
If a caster wants to make a \textit{Potent Sleep} spell, this would raise their Intelligence by 1 (which helps casting the spell), then raise the \gls{tn} from 7 to 8, making the endeavour pointless.
}{
This includes \textit{Pocket Spells}, \textit{Talismans}, and \textit{Artefacts}.
}

\spell{Spell Breaking}{Instant}{Sphere Rating}{Destroy a spell}

The caster can destroy an existing spell, whether that spell is a persistent effect, such as a Polymorph, or a magical item.
The spell requires an opposed roll of Intelligence + the sphere being used.

\iftoggle{verbose}{%
For example, a priest casts an Aldaron spell.
She has Intelligence +2 and Aldaron 3.
The \gls{tn} is therefore ($7+2+3=$) 12.
Later, an alchemist attempts to dispel the magic.
He rolls with his Intelligence Bonus of +3, but he does not have the Aldaron sphere, so he can add nothing more.
If he fails the roll, he can attempt to try again, turning this into a ritual spell.
However, if that fails, he simply cannot roll again.
}{}

\spelllevel

\enhancement{1}{Subtle}{Cast any spell unseen}

Casting an illusion or enchantment on someone with a flashing, loud and generally obvious spell can be quite a give away.
Any caster can attempt to cast a spell while simply whispering and moving their hands slowly and subtly.

People around the mage can still sometimes spot a spell being cast. They use their Wits + Academics in a resisted roll against the mage's Dexterity + Deceit.

\label{pocketSpell}
\spell{Pocket Spell}{Instant}{Crafts}{Allow a mana stone to cast a single spell at the cost of 1 permanent \gls{mp} from the mana stone, after which the stone is broken}

This spell permanently reduces the \glspl{mp} in a mana stone by 1, and allows the mana stone to permanently use a single spell which the mage casts immediately after this spell.

Once the mana stone is a pocket spell, it can activate once, and then the mana stone becomes useless, and can never contain \glspl{mp} again.
Some alchemists create scrolls which become useless once read.
Some priests of \gls{naturegod} enchant animals with a single spell, just to see how the animal will use it.
The only limitation is that the mana stone must have enough \gls{mp} to cast the spell once.

\iftoggle{verbose}{
The pocket spell always produces a single effect.
A scroll of illusion might cast an illusion of a griffin, but cannot cast `just whatever the user desires'.
Any continuous effects last for a number of scenes equal to the mage's Intelligence Bonus.
}{}

These magical items are activated by a `command word'.
Command words do not necessarily have to be actual words -- they could be entire phrases, gestures, or even thought-patterns.

Whoever activated the item counts as `the caster', for the purpose of casting a spell.
\iftoggle{verbose}{%
  For example, a spell which allows `the caster' to detect what is happening far away, could be placed into an alchemical scroll.
  Whoever uses the scroll would then be able to detect the area.
}{}%
If the item changes hands while the spell is active, `the caster' changes too.
\iftoggle{verbose}{%
  If a magical item allows `the caster', to enchant people, then whoever holds the item at that moment counts as `the caster', even if the item changes hands several times.
}{}

Pocket spells require 4 \glspl{ap} to cast, just like using any other item.

These items always use the caster's Intelligence Bonus, but have no relation to Skills, so they do not gain any Bonus from anyone's Skills.
\iftoggle{verbose}{%
  When casting a scroll with a Fireball spell, if the caster's Intelligence Bonus were +1, the scroll rolls to hit with +1 Bonus, even if neither scroll's creator, or user, have the Projectiles Skill.
}{}

The item's effective Wits Bonus is also equal to the caster's Intelligence, so a caster with Intelligence +2 would make items which have an effective Intelligence of +2 and Wits +2.
This is known as the item's `\textit{Potency}'.
\index{Potency (for magical items)}

All Pocket Spells last a number of scenes equal to the caster's Intelligence Bonus at the time of casting.

\spelllevel

\enhancement{1}{Ranged}{Increase any spell's range to line of sight}
\label{ranged}

Any spell, from any sphere, can be targeted anywhere the mage can clearly sense, breaking all the normal range boundaries of spells.

\spelllevel

\spell{Talisman}{Instant}{Academics}{Sacrifice one permanent \gls{mp} in a mana stone to let it cast a spell}
\label{talisman}

The mage takes a mana stone and allows it to cast a spell, forging a new magical item. A sword could be made which can summon blinding light, or a ruby could be infused with the power to teleport the caster to a specific nearby location.

Talismans work exactly like Pocket Spells, except that the item regenerates 2 \glspl{mp} per scene, and can be recast.
Any number of spells can be cast into the item, so long as each one is implanted within the same casting.
Each spell removes 1 \gls{mp} from the Mana Stone's total.

Magical items continue to store \gls{mp} for use by people on that Path of Magic.

Such basic spells always take effect in exactly the same way and use the mage's stats for any rolls. A second level Aldaron spell set to freeze water will always do just that, and can never cast \textit{Light}.
An illusion-generating mask, making the wearer into a bush, will always turn that wearer into a bush, regardless of what the user may want the illusion to be of.

Talismans which do not have enough mana simply fail to cast.
The one exception here is the Path of Song, wherein spell-songs which have too much mana drawn from them simply break, rendering the Talisman-song useless.

\enhancement{1}{Sentient}{Allow the spell to make its own decisions}
\label{sentient}
Powerful \glspl{miracleworker} can make their spells sentient.
This does not affect the \emph{target} of the spell (enchanted dogs do not become suddenly self-aware), but the spell itself.

Sentient spells can `miss' a caster's allies, so a sentient spell to freeze over a river would target only the caster's enemies, or a \textit{Wide, Sentient Fireball} would burn only enemies.

Sentient illusions can independently walk, talk, and use any languages the caster knows.
These spells are never fully-fledged people, but simple, often mute, stereotypes.

Sentient Force spells, such as levitation, allow a spell to independently decide where an item will be lifted, or even attack with Dancing Swords.
However, \textit{Wide, Sentient} spells do not gain multiple consciences -- they can only focus on one thing at a time, just like a normal person.
\iftoggle{verbose}{%
  A \textit{Wide, Sentient, Dancing Swords} spell which levitated 4 daggers would not be able to make 4 attacks per round.
}{}

Sentient spells take the Mental Attributes of their caster, so they can enter combat using their own \glspl{ap}, without bothering the caster.
They also take their caster's Code, so a mage devoted to \gls{justicegod} would create spells which seek to enforce the local laws.

\iftoggle{verbose}{%
  The most common use of a sentient spell is to create a Pocket Spell, Talisman, or other magical item which can make its own decisions about when to cast.
  A sword which casts fireball upon enemies when striking, or a tree which shines with light when a human enters the area, or a runic enscription on a stone door that turns the stone back to wood once the password is stated are all possibilities.
}{
  Sentient Pocket Spells, Talismans, and Artefacts can cast their own spells.
}

\spelllevel

\enhancement{2}{Massive}{Increase the target area until it's the size of \glsentryshort{lv} + Wits areas}
\label{Massive}

The spell spreads across a massive space -- indoors this could be multiple rooms, outdoors it could be a field, or a massive segment of a forest.
Massive spells target a number of areas equal to the spell's level plus the caster's Wits.
\iftoggle{verbose}{%
This spell has the usual range, so if a caster cannot also cast at long-range, the spell must begin in the area the caster currently stands.
}{}

\spelllevel

\enhancement{1}{Eternal}{Make any magical item's effects permanent}

This enhancement affects spells imbued in magical items such as Pocket Spells, Talismans, and Artefacts.
The resulting spell lasts until it is cancelled by a command word, rather than dying after a number of scenes equal to its Potency.

If the item has multiple spells, only one becomes \textit{Eternal}.

\iftoggle{verbose}{
If an \textit{Eternal Talisman} or other item allows the caster to control undead and then changes hands, the control of the undead changes with it, because the effective `caster' is just whoever holds the item at the time.
}{}

\enhancement{Varies}{Combined}{Add a second spell effect to the target}

A secondary spell can be combined to throw at the same target.
The combined spells have a single target.

The smaller spell adds a number of effective levels equal to half its own level, and the total is used to check \gls{mp} and \gls{ap} cost.
As long as all spells could be cast normally, the maximum level is determined by the mana score.

\iftoggle{verbose}{
  For example, a sorcerer with elven heritage attempts to cast a \textit{Wide Air Bubble} alongside \textit{Wide Mage Armour}, on all of her companions.
  The highest level used is \textit{Mage Armour}, at level 3.
  \textit{Wide Air Bubble} is level 2, so half of that is 1.

  The effective level is 4 (even though she only has Aldaron at level 3), so the spell costs 6 \glspl{ap} to cast, minus her +1 Wits Bonus.
}{}

\spelllevel

\spell{Artefact}{Instant}{Academics}{As `Talisman', but the item gains a full sphere of magic}\\
This functions just like the Talisman spell, except that the mage can imbue a full sphere's level.
If the item has Necromancy level 2, the item can cast any Necromancy spell of level 2 or less.
If it has Invocation level 3, it can cast any spell at level 3 or less.
Each sphere (but not each level) reduces the item's \gls{mp} by 1, just as with \textit{Pocket Spell}.
The item's user simply focusses on what they want, and the spell casts.

\iftoggle{verbose}{
  Once the spell Artefact spell is cast, the spell's level must be cast into it as part of the same spell.
  Therefore, a mage creating an item with the 3rd level of Necromancy must first spend \arabic{spelllevel} \glspl{mp}, followed by 3 \glspl{mp} to place the necromancy sphere into the item.
}{}

\spelllevel

\enhancement{5}{Exceptional}{Make Mana Stones regenerate and store 3 \glspl{mp}}

Mana Stones, Talismans, and Artefacts cast with this enhancement can store 3 \glspl{mp} for each level of the spell, rather than the usual 2.
They also regenerate 3 \glspl{mp} per scene.

\end{multicols}

\section{Mana Stones}

\begin{multicols}{2}

\noindent
Every Path has its own way of creating magical items.
These magical items always begin life as a `mana stone' -- i.e. a thing which can store mana, and produce it upon command.
Alchemists typically use gold, while rune-casters use any surface which can hold a rune upon it.

The mana stones often hold spells, which they can summon on the command of the user (or sometimes, based upon the mana stone's own desires).
When \glspl{miracleworker} walk multiple Paths of magic, they can create mana stones which combine spells, by creating stones which work with both schools.
For example, an alchemical rune-caster might carve runes into gold plate in order to complete a spell.

\subsection{The Path of Alchemy}
\index{Alchemy}

\index{Mana Stones!Alchemy}

Alchemical mana stones are always precious items, such as gold, rubies, or diamonds.
A mana stone costs 10 gp per \gls{mp} which can be stored inside it, so a mana stone storing 3 \gls{mp} would cost 30 gp.
The exact item might be a simply ruby which stores mana, a diamond-headed wand of ivory which blasts out fireballs or a sword with jewels on the handle which surrounds the warrior with moving illusions of their.
Alchemical mana stones with a spell always activate those spells with a command word.

Such mana stones can be reworked to make other items, but can only store fewer \glspl{mp} than the first time they were used.

\subsection{The Path of Blood}

\index{Mana Stones!Blood}

Those with magic flowing through their own blood can only use themselves as mana stones.
They can store magic in their heart, their fingers, or eyes, but can never use an external item to store mana.

\subsection{The Path of Devotion}


Each type of devotion has its own mana stone.
See the individual references in chapter \ref{gods_codes}.

\subsection{The Path of Runes}

\index{Mana Stones!Runes}

Rune casters' mana stones are, of course, runic carvings, and can never be painted onto anything.
Such mana stones can store a number of \glspl{mp} equal to double the item's \glsentryname{weightrating}, so an item with a \gls{weightrating} of 3 could store up to 6 \glspl{mp}.

\subsection{The Path of Song}
\label{song}
\index{Mana Stones!Song}

The mana stones of the Path of Song are actual songs.
The bard composes a song especially for the purpose; when anyone -- anywhere in the world -- plays the song on the correct instrument the mana can be regained.

If anyone ever pulls mana from the song (either for a spell casting or because they are low on mana) while the song-spell is empty, it is destroyed forever.
The song will be difficult for anyone to remember and will no longer store any mana until someone remakes the spell.

Rare and powerful spell-songs are swapped as currency among bards -- spells which can protect the singer or enchant a crowd.


\end{multicols}

