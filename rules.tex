\chapter{The Rules}
\label{coreRules}

\settoggle{bestiarychapter}{true}

\section{Basic Actions}
\label{basicaction}

\begin{multicols}{2}

\newcommand{\TNChart}{

  \begin{rollchart}

    {\bf \glsentrytext{tn}} & {\bf Task} \\\hline

    2 & Automatic \\

    4 & Trivial \\

    6 & Easy \\

    8 & Serious \\

    10 & Professional \\

    12 & Specialist \\

    14 & Extreme \\

    16 & Epic \\

    18 & Legendary \\

    20 & Implausible \\

  \end{rollchart}

}

\iftoggle{verbose}{%
  \sidebox{
  \TNChart
  }
}{
  \TNChart
}

\noindent
A basic action is performed by rolling $2D6$ equal or higher than the \gls{tn} for the action.
The more difficult the action, the higher the \gls{tn}.
Players add their character's Attribute and \gls{skill} to the roll.
Attributes and \glspl{skill} usually go as high as +3, so a +6 bonus is possible, and higher bonuses are possible with knacks other bonuses.

Poor Attributes give a penalty, rather than a bonus.

\iftoggle{verbose}{All actions are assumed to have a \gls{tn} of 7 unless your \gls{gm} states otherwise.
Don't ask -- just roll!

\input{story/4-rolls.tex}

}{}

\subsection{Group Rolls}
\label{grouproll}
\index{Group Rolls}

If the party are all attempting the same action, then they only make one roll, while adding different attributes to obtain their individual result.
\iftoggle{verbose}{%
If the party want to cross a raging river, then the roll of Strength + Athletics is made.
Let's say the party's lead warrior has a total bonus of +5, the next has +2, and the party's alchemist has a total of -1.
They roll the dice, and come up with `8'.
That gives the first character 13, the second 10, and the mage gets 7.

Broadly, if everyone acts upon the same thing, the group only makes one roll.

\needspace{4em}
Example Group rolls include:

\begin{itemize}

  \item{Busting open a door}
  \item{Recalling an obscure political fact}
  \item{Sneaking past guards}

\end{itemize}
}{}


\subsection{\glsentrytext{restingaction}}\label{restingactions}

The basic system assumes that actions are taken while the player is being hurried.
When taking one's time is possible, things become much easier.
Exactly how much time is required is up to the \gls{gm}, but it can easily be several nights.
Sneaking into a house is a challenge, but much easier when one can take one's time, looking at it night after night to see if there is any breach in security.
Getting the latest gossip from a new village in a night is a normal action, but staying for a week and drinking every night with a different local is a \gls{restingaction}.

When taking a \gls{restingaction}, one die is presumed to have rolled a 6 and the player simply rolls the other die to obtain a result.

\iftoggle{verbose}{

  Example \glspl{restingaction} include:

  \begin{itemize}

  \item Studying a magical artefact for a month.
  \item Repeatedly attempting to gain an audience with a local noble.
  \item Bashing away at a door for a month, using different tools.
  \end{itemize}

  \input{story/5-resting.tex}

}{}

\subsection{Teamwork}
\label{teamwork}
\index{Teamwork}
\index{Group actions}

Some tasks lend themselves to working with others. Others can be difficult or impossible to do with companions. Some tasks, such as fleeing or sneaking, do not benefit at all from having a load of friends right behind you.

When acting as a group provides no benefit, one player rolls the dice and the same result counts for everyone.  If that player rolls a 9, then everyone's score is 9 and they add their own bonuses and penalties.

If, on the other hand, working together can benefit a situation, one character takes the lead, and up to three other characters can add up to half their bonus (rounded up).
Two companions with a +3 bonus would add a total of a +2 bonus.

\iftoggle{verbose}{

  Example Team Actions include:

  \begin{itemize}

  \item Getting a broken cart down a hill without damaging it.
  \item Tracking down a local thief in a large city.
  \item Spotting danger in the wild.

  \end{itemize}

  \input{story/6-team.tex}
}{}

\subsubsection{Stacking}
\index{Stacking}
\label{stacking}

In general, whenever you want to see how something stacks, add the second lot as half its usual value.
If two people are pushing with Strength +2, they count as having a total Strength of +3.
If others want to join, add any third items as worth a quarter, then an eighth, and so on.

\subsection{Resisted Actions}
\index{Resisted Actions}
\label{resistedactions}

When \glspl{pc} come into disagreements with \glspl{npc}, the \glspl{pc} pick up the dice, and roll.  The \gls{npc}'s Traits add to the \gls{tn}.

For example, if a player attempts to pick someone's pocket, the \gls{npc} never rolls Wits + Vigilance.
Instead, if an \gls{npc} has a Wits + Vigilance total of +4, the \gls{tn} for the roll is $7 + 4 = 11$.
The player then rolls against that \gls{tn}.
The results are exactly the same, but having Players roll for everything helps emphasize agency and can speed up the game.

\iftoggle{verbose}{
  \input{story/7-resisted.tex}
}{}

\subsection{Margins}
\index{Margins}
\index{Failure Margin}
\label{margin}

Most actions are either a success or a failure, but sometimes the \gls{gm} will request to know a roll's Margin -- i.e. just how well the character has succeeded at the task. The Margin is the number of points over the \gls{tn} a roll has gathered. If the \gls{tn} is 9 and a player scores 11 then the Margin is 2.

The \gls{gm} might use a Margin for some variable, for example a bard attempting to charm a crowd into giving him money might gain $2D6$ copper pieces plus the Margin, so if the Margin is 3 then he would get $2D6+3$ copper pieces.
Margins might also be used to gain bonuses on later rolls.
Someone attempting to impress a noble court might roll Charisma with the Tactics Skill; the bigger the Margin the more troops they will be trusted with.

\iftoggle{verbose}{
  \input{story/8-hiding.tex}
}{}

\iftoggle{verbose}{
\subsection{What the Dice Mean}

You might think of the dice as representing random chance in the environment. Just how irritated is that person you're trying to question, and how creative is that craftsman feeling today? Dice are never re-rolled for different results on the same action because once the dice have told you what the situation is, the situation stays put.

Such a do-over still suggests initial failure; it just means that the character is trying over and over again until a better result is obtained.
}{
\subsection{No Rerolls}
Characters attempting to change a Standard Action into a \gls{restingaction} do not reroll but rather keep the same roll and turn one die up to show a 6.
}%
Actions cannot be attempted multiple times with rerolls unless the situation has changed notably.

\end{multicols}

\section{Experience}
\label{xp}
\index{Experience Points}

\XPchart

\begin{multicols}{2}

\noindent
As the story progresses, the \glspl{pc} gain \glsentryfullpl{xp}.
Each part of the character can be improved by spending \gls{xp}.
Buying basic stats is cheap while higher level stats quickly become extremely expensive.

\subsubsection{Starting \glspl{xp}}

Characters begin play with an amount of \glspl{xp} stipulated by the \gls{gm} depending upon the level of their campaign.
The suggested starting \glspl{xp} is 50, with up to 150 \glspl{xp} for more advanced campaigns.

\subsection{Gaining \gls{xp}}

Players receive \gls{xp} from the \gls{gm} for killing monsters, fulfilling their codes, and completing missions.
Larger and more dangerous monsters garner more \gls{xp}, as do grander missions.
\iftoggle{verbose}{%
The personal goals and piety of a character are denoted by different codes of belief and gods.
See page \pageref{gods_codes} for details on the gods and personal codes of honour.

\iftoggle{verbose}{
  Players also gain \glspl{xp} for spending \glspl{storypoint} (see page \pageref{storyXP}).
}
{}

\subsubsection{Training Time}

The \gls{gm} may wish to only award \gls{xp} at the end of a session, and may restrict when it can be spent.
Each Trait should increase by no more than a single level during the course of one \gls{adventure} -- you might be lucky enough to get enough \gls{xp} to raise your Strength from -2 to +1 in a single session, but nobody can accrue that kind of muscle mass in such a short period of time.
}{}

\subsubsection{What Counts?}

Enemies don't have to be killed for the \gls{xp}, merely defeated.
Any enemies fleeing count for half their \gls{xp} value so long as they engaged in one round of combat.

\subsection{Experience Points \& the Discount}

\iftoggle{verbose}{

Standing alone against a towering ogre is a nightmare, but three warriors standing against three ogres can be much easier.
A battle against thirty goblins can really take its toll, but three different battles against ten goblins can be child's play.
To represent this, we have \textit{the \gls{xp} Discount} -- a price you pay for every member of the party.

\sidepic{Roch_Hercka/xp-1}{\label{roch:xp1}}


For every member of the party, that many points are deducted from one monster's \gls{xp} value (to a minimum of 0).
If the party has two members, the first two monsters have 2 \gls{xp} deducted from their total value.
If the party has five members, the first five monsters have 5 \gls{xp} deducted from their total.

\iftoggle{verbose}{

If a single warrior defeats a dragon worth 22 \gls{xp}, then the warrior receives 21 \gls{xp}, because 1 \gls{xp} is removed from the total.
If he fights 10 ghouls worth 2 \gls{xp} each, then he receives 1 for the first, and 2 for the rest, for a total of 19 \gls{xp}.

\sidepic{Roch_Hercka/xp-2}{\label{roch:xp2}}
}{}

However, if five characters are fighting the 10 ghouls together, they each deduct 5 \gls{xp} from a single monster.
The first five ghouls are worth nothing, because each net ($2 - 5 = $) 0 \gls{xp}.
Only the last 5 ghouls count, bringing 10 \gls{xp} in total.  Dividing this among 5 players, each receives 2 \gls{xp} at the end.

}{}

If players need to discount multiple adversaries, they are counted from highest to lowest \gls{xp} value.

\subsubsection{Mass-Damage Discount}
\label{xpCreatureMax}

\iftoggle{verbose}{%
  Sufficiently powerful characters can take out entire armies with a single spell.
  However, only the first 10 enemies killed by a single action give \glspl{xp}.
  Any further enemies give no \glspl{xp}.
  Killing 80 goblins with a \textit{Massive Fireball} grants the same \glspl{xp} as killing 10.
}{
  Players may receive \glspl{xp} for killing up to 10 creatures within a single action.
  The highest \gls{xp} total is taken first, then the second highest, and then the third.
}

\subsection{Spending \gls{xp}}

At the end of each session, players can spend their \glspl{xp} to improve their characters.
Spending \glspl{xp} suddenly can only be done by spending a \gls{storypoint}.%
\footnote{\nameref{surpriseSkill} story is on page \pageref{surpriseSkill}.}
Each additional level of a Trait has a steeply progressive cost.
The costs represent buying the next level; the first level of a school of magic costs 10 and the second costs 15 -- buying up to the second level costs 25 \gls{xp} in total.
Knacks work similarly, where the first Knack costs only 5 \gls{xp}, but the second Knack a Player purchases costs 10, and so on, with each additional Knack costing an additional 5 \gls{xp} cumulatively.

Attributes have a standard maximum of +3 and minimum of -3. This is adjusted by race, so for instance elves have a +1 bonus to Wits but -1 to Strength, so their maximum Strength score would be 2 and the minimum -4, while the maximum Wits is +4 and the minimum -2.

Buying off a negative level increases it by 1 and always costs 5 \gls{xp}, so taking a character from -4 Strength to 0 would cost 20 \gls{xp}.

\end{multicols}

\section{Gold \& Goods}
\label{goods}
\index{Equipment}

\begin{multicols}{2}

\subsection{Money}
\index{Money}

An open ended list of equipment is provided to give a basic idea of costs.
The basic coinage covered here is human coinage, but each culture will use their own currency and exchange rates.
A hundred \glsentryfullpl{cp} is worth 1 \gls{sp}.
10 \glspl{sp} is worth 1 \gls{gp}.

An average villager will make little spare money -- perhaps 10 \glspl{sp} in a year if they bother to save.
Sellswords can expect to make upwards to 10 gold per year if they are hired by a villagemaster.
The average free trader -- a blacksmith or cloth dyer -- can expect to make 5 \gls{sp} in a month.

Prices for weapons are placed next to the weapon in chapter \ref{combat}, page \pageref{weaponschart}.

\begin{boxtable}[XcX]

  \textbf{Animal} & & \textbf{Cost} \\\hline

  Dog & & 2 \gls{sp} \\

  Donkey &   &  2 \gls{sp} \\

  Horse &   &  5 \gls{gp} \\

  War Horse &   &  8 \gls{gp} \\

  Leather Barding &   &  1 \gls{gp} \\

  Chain Barding &   &  2 \gls{gp} \\

  Plate Barding &   &  15 \gls{gp} \\

\end{boxtable}

\begin{boxtable}[XcX]

  \textbf{Buildings} & & \textbf{Cost} \\\hline

  Cottage & &  20 \gls{gp} \\

  Keep & &  1,000 \gls{gp} \\

  Small Castle & &  4,000 \gls{gp} \\

  Medium Castle & &  10,000 \gls{gp} \\

  Large Castle & &  30,000 \gls{gp} \\

\end{boxtable}

\begin{boxtable}[XcX]
  \index{Clothes}

  \textbf{Clothing} & \textbf{Weight} & \textbf{Cost} \\\hline

  Peasant clothes &  -3 &  50 \gls{cp} \\

  Noble clothes &  -4 &  1 \gls{gp} \\

  Lavish clothes &  -5 &  3 \gls{gp} \\

  \label{warmClothes}
  Warm clothes &  -3 &  5 \gls{sp} \\

\end{boxtable}

\index{Food}\index{Rations}
\begin{boxtable}[XcX]

  \textbf{Professional Tools} & \textbf{Weight} & \textbf{Cost} \\\hline

  Grappling hook &  -2 &  1 \gls{sp} \\

  Ink bottle &  &  2 \gls{sp} \\

  Iron rations &  -2 &  10 \gls{cp} \\
  

  Lantern &  -2 &  3 \gls{sp} \\

  Lock pick set &   &  10 \gls{sp} \\

  Metallurgy set &  6 &  40 \gls{sp} \\

  Parchment sheet &   &  1 \gls{cp} \\

  Quill &   &  4 \gls{cp} \\

  Rope, 50' &  -1 &  2 \gls{sp} \\

  Rope, silk, 50' &  -4 &  3 \gls{sp} \\

\end{boxtable}

\index{Camping Equipment}

\begin{boxtable}[XcX]

  \textbf{Travel} & \textbf{Weight} & \textbf{Cost} \\\hline

  Alcohol lantern & -5 & {4 \gls{sp}} \\

  Cart & 10 &  1 \gls{gp} \\

  Camping equipment & 1 & {3 sp} \\

  Torch & -4 & {8 \gls{cp}} \\

\end{boxtable}

\subsection{Working Beasts}

Animal stats vary, but you can use the below as a go-to standard for working animals.
Quadrupeds can run at double the standard speed when going full pace, so horses can allow a party to travel at far higher speeds than normal.

\settoggle{examplecharacter}{true}
\horse

Characters can significantly improve their average travel-times with a horse, covering 25 miles per day without tiring.

\settoggle{examplecharacter}{true}
\warhorse

War horses aren't much faster than regular horses, but they won't become so easily frightened when danger approaches.

\settoggle{examplecharacter}{true}
\huntingdog[\npc{\A}{Hunting Dog}]

Hunting dogs are mostly useless in warfare, but they make excellent watchmen.

\iftoggle{verbose}{
  \pic{Roch_Hercka/dwarf_encumbrance}{\label{roch:dwarf}}
}{} 

\subsection{Weight \& Encumbrance}
\index{Weight}
\index{Encumbrance}
\label{weightrating}

We measure weight in broad terms.
Characters have a \glsentryname{weightrating} equal to their \glspl{hp}, so elves tend to have 5, while humans tend to have a \gls{weightrating} of 7.
Items work similarly, with \gls{weightrating} between -4 (for very light items), through +11 (for wardrobes, carts, and boulders), and so on.

If an item's \glsentryname{weightrating} is equal or below your character's Strength, you can lift it easily.
However, if the items has a greater \gls{weightrating} than your Strength Bonus, you gain a point of Encumbrance for every increment that item is above your Strength Bonus.
Encumbrance slows you down and makes you tired, detracting from your Speed Bonus, and adding to your \glspl{fatigue} each Scene.

Characters can carry items with a maximum \glsentryname{weightrating} of their Strength Bonus plus 6, so a man with 7 \gls{hp} could only be carried with a Strength Bonus of +1 or greater.
Depending upon the circumstances, the \gls{gm} may allow heavier objects to be dragged or rolled.

Items carried in only one hand count as having +2 to the \gls{weightrating}, so hefting a battle axe in only one hand would mean it has an effective \gls{weightrating} of 5.

Characters cannot carry any item which gives them a -5 Encumbrance rating or higher.
They can, however, drag items with up to a \gls{weightrating} of up to 10 points above their Strength Attribute (rough surfaces can increase the requirement substantially).

\iftoggle{verbose}{
  \begin{figure*}[b!]
  \servicesChart
  \end{figure*}
}{
  \begin{figure*}[t!]
  \footnotesize
  \servicesChart
  \end{figure*}
}

\iftoggle{verbose}{
  \input{story/encumbrance.tex}
}{}

\iftoggle{verbose}{
  \input{story/equipment.tex}
  \input{story/cc.tex}
}{}

\subsection{Services}
\label{services}

Money can buy you more than things.  In fact, for the right money in a large city, characters can buy a full entourage.  Villages, however, will not admit of the same opportunities.

The costs above show the starting price for a few services, plus additional fees for the details.
For example, hiring a guide for an uncharted and dangerous area for 5 days would cost 800 \glspl{cp}.

\iftoggle{aif}{
  The \gls{guard} regularly form an exception to these service charges, as people expect them to work wherever \gls{king} sends them, so they regularly undercharge or simply refuse to work.
}{}

Hiring someone generally requires a Wits + Empathy roll, \gls{tn} 7, to determine their capability.
Failure means that this person is useless.
Perhaps they want to work with you because they have no idea how bad they are at their job, or perhaps they simply want to rip you off.

The Failure Margin should indicate just how bad the henchman is, so the \gls{gm} is encouraged to make the roll in secret.

\subsection{Cultures \& Exchange Rates}
\index{Exchange Rates}

Different cultures have different exchange rates -- the elven versions of standard equipment are always artistically engraved and in high demand; the elves also value the coinage and materials of outsiders very little, so they will not part with their items for human or dwarvish gold easily.
As a result, their -- and other -- culture's items are more expensive than human items.
\sidebox{
  \begin{rollchart}
    Race & Multiplier \\\hline

    Elves & $\times 3$ \\

    Dwarves & $\times 2$ \\

    Gnomes & $\times 2$ \\

    Gnolls & $\frac{1}{2}$ \\
  \end{rollchart}
}


The costs of the items here are based on the most common race -- humans.
Other races have a multiplier effect based on how expensive their equipment is.

Different races will also have different items available.
In general, anything of a basic (non adjusted) value of over 2 \gls{sp} will not be available in a village, while towns will not have anything of over 1 \gls{gp} in value.
Characters can only buy expensive, artisan, items in cities.

Services suffer the same adjustments.
The party will not find gnomes willing to guide `a bunch of giants' as easily as the young humans in a village.


\end{multicols}

\section{Time \& Space}

\begin{multicols}{2}

\noindent
This game uses the entirely abstract measurements of the `scene' and `square' for time and space. They are more compliant to narrative than physics, and form the basis of all movement and actions whenever people start tracking how long something takes and where everyone is.

\subsection{Time as Scenes}
\label{time}

\subsubsection{Rounds}

When everyone wants to talk and act at the same time, time is tracked in \glspl{round}.
This period of time is used almost exclusively while tracking combat.
The \gls{round} itself can then be further divided into \glspl{ap} if you want real detail, but that's covered later.
All that matters is that a \gls{round} is a period of time in which people attempt to hit each other, then another \gls{round} occurs.

\subsubsection{Scenes}

Most of the time, actions will not occur through \glspl{round} but rather scenes. A scene is just any unit of time in which the \glspl{pc} take on a task or two, usually within a single area. We track scenes only because a few game effects occur at the end of each scene -- mostly these are narrative effects such as regaining \glspl{fp}\footnote{See page \pageref{fate_points}.} in order to regain plot-immunity from Damage. The scene lasts until the \gls{gm} says that it's over.

\subsubsection{Day}
\label{daytimes}

We divide days into four parts -- morning, afternoon, evening and night.
These areas are broadly there for rests -- anyone resting for one of these periods can heal \glspl{fatigue}.%
\footnote{\Glspl{fatigue} are covered on page \pageref{fatigue}.}

\paragraph{Travel}
\index{Marching}
\index{Travel}
happens just as fast as players like.
If they want to march 10 miles in a morning, they can -- they have no hard limit.
Of course after 10 miles, they will have 10 \glspl{fatigue} (except humans, who will have 5), and have to stop for a rest.

\subsubsection{The \Glsentrytext{adventure}}

The \gls{adventure} lasts until the current plot-thread is resolved, or some period of `sandboxing' through a world until a proper use of one's time can be found.
At the end of every \gls{adventure}, \gls{downtime} should be called, and all characters should heal all \glspl{hp}, \glspl{mp}, \glspl{fp}, and \glspl{fatigue}.

\subsubsection{\glsentrytext{downtime}}

\Gls{downtime} is when the current stories come to a close and the \glspl{pc} take a rest.
\iftoggle{verbose}{%
  This non-\gls{adventure} period allows the \glspl{pc} to heal, and advance Traits.
  It can be weeks, years, or even decades.
  The party can declare \gls{downtime} at any point once the \glspl{pc} have reached a safe area, although the \gls{gm} is free to interrupt that \gls{downtime} with events.
  Likewise, the \gls{gm} can declare a \gls{downtime} at any point, but the players can interrupt this with personal missions.
}{
  After downtime, \glspl{pc} receive an amount of \glspl{storypoint} equal to the square root of the number of years spent.
}

\subsection{Space as Squares}\index{Space}\index{Squares}\index{Areas}
\label{space}

\subsubsection{Squares}

Space is tracked through \glspl{square}.
A \gls{square} is just any unit of space within the battlefield.
If you are using a battlemap which has squares marked out on it, then those squares are the size of a square, even if those squares happen to look very hexagonal.
A square might be ten metres wide as each one covers an entire house when the battlefield is a large town, or it might be just two yards wide when moving through a detailed map of a dungeon.
The precise distances represented do not matter, just so long as they consistently balance one character's ability to run away with another's ability to hit someone with a projectile.

\subsubsection{Areas}

An \gls{area} is just any place which looks different from another.
While traipsing through a small dungeon, each room and cavern entered might be thought of as an \gls{area}.
When gallivanting through open plains one \gls{area} might be a copse of trees, another a lake, and then the next area a village.

\subsubsection{Region}

Regions encompasses a full forest, a town, or a collection of villages.
Each region has its own set of likely encounters, such as tradesmen in the villages, cut-throats in town, and elves in the forest.%
\iftoggle{verbose}{%
\footnote{If all this looks like a repugnant abstraction, just set a square to two yards, an area to one mile, a \gls{round} to six seconds and a scene to one hour.}
}{}

\end{multicols}

\settoggle{bestiarychapter}{false}
