# BIND

|             Downloads                |         Questions             |
|:-------------------------------------|:------------------------------|
| [Core Book][core download]           | [Wiki][wiki]                  | 
| [Character Sheet][cs]                | [Email an idea][issuesEmail]  |
| [Reference Version][reference]       | [Issues Board][board]         |

BIND is a fantasy tabletop RPG core ruleset.

# Rules

All rules are geared towards fast resolutions and player choices.

- One roll combat resolutions:
    * Attack and defence are the same roll.
    * If you miss, the enemy damages you.
    * No initiative rolls - just go!
    * Whoever has the most Action Points can interrupt and demand to go first.
- GMs don't roll much, so they can focus on orchestration.
- Spellcasters spend Mana Points.
- Character creation takes 15 minutes.
    * Roll a random race and Attributes.
    * Pick a 2-word concept.
    * Spend your XP on Skills.
- Leave the backstory till the game starts.
    * Spend your 5 Story Points to introduces allies, safe locations, and languages from your past.
    * When your character dies, pick one of the allies introduced through another story, and go from there.

# Setting

Make your own, or print a copy of [Adventures in Fenestra](https://gitlab.com/bindrpg/aif).

# Compiling

The dependencies are

- `texlive-full` (or `texlive-most` on some distributions)
- `git-lfs`
- `inkscape`
- `make`

To make the document, simply enter the directory, and type `make`.

Finally, take the pdf to your local, friendly, printing store, and have them make you a nice hardback copy.

# Get Involved

The book is under an open source licence, so whoever plays it decides how it gets to work.
If you want to get involved, there are a number of ways to contribute:

- Emailing ideas [here][issuesEmail].

- Posting ideas on the [board][board].

- Playtesting.

- Contributing art.

- Fork the project, and start fixing issues.

- Or if you don't like how I'm handling the book, fork it and make your own.

[core download]: https://gitlab.com/bindrpg/core/-/jobs/artifacts/master/raw/BIND.pdf?job=compile_pdf
[reference]: https://gitlab.com/bindrpg/core/-/jobs/artifacts/master/raw/BIND_ref.pdf?job=compile_pdf
[resources]: https://gitlab.com/bindrpg/core/-/jobs/artifacts/master/raw/resources.pdf?job=compile_pdf
[wiki]: https://gitlab.com/bindrpg/core/-/wikis/home
[aif]: https://gitlab.com/bindrpg/aif
[cs]: https://gitlab.com/bindrpg/core/-/blob/ods/calc_cs/bind_cs.ods
[board]: https://gitlab.com/bindrpg/core/issues
[issuesEmail]: mailto:incoming+bindrpg-core-16324687-issue-@incoming.gitlab.com
