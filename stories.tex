\chapter{Stories}
\label{stories}
\index{Stories}

\iftoggle{verbose}{
  \widePic{Vladimir_Arabadzhi/escape}
}{}

\begin{multicols}{2}

\noindent
Players `write' most of their backstory during play rather than before it.
\Glspl{pc} can start off as blank slates with no history, but the history comes out of the woodwork soon after as players can spend 5 \glspl{storypoint} to bring their history into the current \gls{adventure}.
Let's look at an example:

\iftoggle{verbose}{
\paragraph{Session 1} has the characters running from the local law.
Jane's player spends 2 \gls{storypoint} and declares \textit{``Luckily, Jane has connections with the local thieves' guild, so she nips into an alley in the slums where the party can lay low for a while''}.

Soon after, the characters need to sell the diamond they've stolen.
Eric knows there are dwarves all around town so he tells the group \textit{``I'll see about help from a local fence.
He's a dwarf I met while lost underground in the Shale, and he knows all the tunnels in the area.''}.

Eric's player spends a \gls{storypoint}.

\paragraph{Session 2} finds the characters lost in deep, long caverns in the bearded mountains, wounded and low on supplies.
Jane's player spends 2 \glspl{storypoint} to declare she knows of a gnomish illusionist who frequents these deep caverns, looking for magical ingredients.

\textit{``How?''}, asks a rather suspicious \glsentryshort{gm}.

\textit{``Well, he used to work with the thieves' guild when he was younger, helping us steal with his illusion magic; sometimes he would give us a magical item which would cast an illusion of something we wanted to steal, so nobody would notice it was missing for a while.
The guild kind of fell apart after he left, which is why it's nothing but slumlords and cutthroats now.''}

En route to a dwarven stronghold with Jane's illusionist, the band are assaulted by a small army of goblins wielding strange magics.
A dwarvish outpost is nearby, so the group run and bang on the great iron gate.  Eric's player spends 2 more \glspl{storypoint}, saying \textit{``This is the place I stayed -- they all know me here.
They should let us in, help us with some supplies, maybe even get me a new sword''}.

At this juncture we know a fair amount about Eric and Jane, where they come from, and who they are, while Sindon the elf continues to be a mystery.

\paragraph{Session 10} comes much later.
The group are lost in a mysterious forest, now teeming with the undead.
Their arrows and rations have run out, the trail leading to the necromancer has gone cold, and they don't think they have the strength to defeat him even if they could find him.

At this point, Sindon reveals that his home village lies nearby.
Everyone can rest there safely, and within the village, they meet his nephew, who will help them find the necromancer by scouting.
Additionally, Sindon's brother can join them, along with his bow.

The various help here costs Sindon all of his 5 \glspl{storypoint}, but the result seems worth the cost.

}{}
\end{multicols}

\section{\glsentrytext{storypoint} Rules}

\begin{multicols}{2}

\noindent
Players begin each with 5 \glspl{storypoint} and spend them at any point during the game.
The encounters must take place in a rational manner -- players might find the perfect sellsword in a town, but if they're in a dungeon, fighting a hall of ghouls, there's little reason for a random sellsword to be present and looking for a job -- this is not an ability to magically summon useful tradesmen with a flash of smoke and plot.
As a result almost all stories will have to be told in populated areas such as towns and villages.

The \gls{gm} is, of course, free to veto any Story suggestions without explanation in order to maintain the integrity of the plot or stop cumbersome play issues.

All stories should be noted down on the back of the character sheet, including any stats from companions, just in case they enter during a later \gls{adventure}.

\iftoggle{verbose}{
  The \gls{gm} may wish to award a couple of additional \glspl{storypoint} over a very long \gls{downtime}.
}{}

\subsection{Experience Points}
\label{storyXP}

Each time a player spends a \gls{storypoint}, the character earns a number of \glspl{xp} equal to the number of \glspl{storypoint} they've spent so far.
\iftoggle{verbose}{%
  The first \gls{storypoint} grants 1 \glspl{xp}, the second grants 2, and so on.
  If a character spends 3 \glspl{storypoint} to stipulate that their blacksmith uncle likes wandering around the depths of the forest, and lives in a small fort, then the player would receive ($1+2+3$) 6 \glspl{xp}.

  }%
  {}
This number remains fixed, even after character death, so if a \gls{pc} dies, the next character can only gain \glspl{xp} from spending \glspl{storypoint} once they have spent the same number as the last character.
\iftoggle{verbose}{%
  If the character above died, they would gain nothing from the first three \glspl{storypoint}, but would then receive 4 \glspl{xp} once they spent the fourth \gls{storypoint}.
}{}

\subsection{Combining Stories}

Whether telling one story each \gls{adventure} or letting everyone know all about your character's backstory all at once, players are encouraged to think about weaving their stories together.
You may have told us that you learnt gnomish when staying with the gnomes.
Now that you need a blacksmith in this village, why not specify that he's a gnome whom you once knew?
And if you need a sellsword to join your group later, how about specifying that you once fought with him to defend the gnomes?

Alternatively, if you are taking out all your stories at once, you might want to declare that you know a mage who lives in a place you can access through a nearby secret portal.
You instantly adopt a safe space and a helpful magical ally, then start expounding upon the days when the alchemist was proudly telling you about his impregnable home.

\end{multicols}

\section{Sample Stories}

\begin{multicols}{2}

\noindent
The following is a suggested list of Stories the players can tell and their costs.
The players are strongly encouraged to suggest more to the \gls{gm} who will either veto them or give them an appropriate cost.

\story{1}{The Second Language}
You have spent a significant amount of time in another culture. You know their language and enough of their background to transfer over basic Skill knowledge. If you have the Performance Skill and are familiar with elvish culture then you also know some Elvish songs.
If you are familiar with gnoll culture and have the Empathy Skill then you know a range of details about gnoll etiquette and lineage.

\iftoggle{verbose}{%
  \begin{itemize}
  \item While the dwarves think they're sneakily planning to stab you in plain sight, you actually learnt dwarvish from a blacksmith dwarf who decided to live among humans.
  \item Back in the circus, the gnomes could never figure out your `elvish arrow trick'.
  You eventually convinced them to teach you how they communicate through whistles in return for teaching them the trick.
  \end{itemize}
}{}

\story{1}{The Surprise Skill}
\label{surpriseSkill}
You have a surprising Skill or Knack which will comes in useful.
As you tell this story, you can buy a Skill level so long as you have the requisite \gls{xp}.
This cannot be a Skill which you have clearly lacked in the past, e.g. if your character has so far been illiterate then you cannot suddenly learn a level of Academics.
However, if you have never wanted for Craft ability then you could declare that you have always known how to forge iron, or that you have the Seafaring Skill.

\iftoggle{verbose}{
  \begin{itemize}
  \item
  \iftoggle{aif}{
    While working as one of the \gls{guard} in the Pebbles islands, you got your sea-legs.
    Swimming through this underground lake shouldn't be a problem.
    You immediately buy two levels of the Seafaring Skill.
    \item
    You lost a younger sibling to a chitincrawler, and now that your troupe has finally come across one, your burning hatred kicks in as you rush forward.
    You purchase the Knack `Chosen Enemy'.
  }{
    You're not one to boss others about, but now you need a plan you may as well pull out the training your brothers taught you about how to set an ambush, and how to wait patiently.
    You buy the first level of the Tactics Skill.
    \item
    Despite your bad manners, you have learnt to read in the thieves' guild some time ago.
    You used to use this skill to make forgeries, but there's nothing in principle stopping you from reading a book.
    You buy the first level of the Academics Skill.
  }
  \item Protecting your siblings from griffin attacks on multiple occasions gave you all the knowledge you need to defend them.
  You reveal you have the Knack `Guardian'.
  \end{itemize}
}{}

\story{1}{The Return}
\label{oldnpc}
You recognise a friendly character from some previous Story you have told.
The \gls{gm} will explain why they are in town but you are free to offer suggestions.
Said characters won't necessarily be as useful as they would be if they were brought into the \gls{adventure} for the first time with Story points and may only help for a scene, but they should be somehow useful.
This may include a trader who was previously known to have valuable information about some situation, or a mage the characters had previously met who could cast a useful spell or two.

This \gls{npc} will probably have gained some \glspl{xp} over this time.
The \gls{npc}'s \glspl{xp} is still equal to half the total \glspl{xp} of whichever party member has the highest \glspl{xp} total.
\footnote{Of course this cannot lower the \gls{npc}'s \glspl{xp}.}%
Any additional \glspl{xp} must be spent immediately (spare \glspl{xp} are discarded), with an explanation about what happened to acquire these new Traits.

\story{1}{The Random Fact}
When the \gls{gm} asks you to make a check to gain knowledge, you can spend a \gls{storypoint} and mention how you know this one particular fact about this topic.
You gain a +6 bonus to a single knowledge check.
This does not count again for the same domain of expertise -- it is only a bonus to knowing one, single fact about the subject.

\iftoggle{verbose}{%
  \begin{itemize}
  \item
  The party want to know how the magical item works.
  You've failed the roll, but then remember you've seen this magical item in your mother's book collection.
  She had extensive shelves full of bizarre tomes, and all the leafing through those tomes finally paid off.
  \iftoggle{aif}{
      \item
      Once, in the \gls{shatteredcastle}, you overheard two guards talking with each other about a door in the hub.
      You know it's in Whiteplains somewhere, and their talk of elves gave you a rough idea of the location.
    \item
    It was unclear if the noble was telling the truth, but you recognise the dyes on his tunic; they come only from the Shale, which can only mean one thing\ldots
  }{
    \item
    The party have you idea where they are, but you suddenly remember your uncle's maps.
    They were always plastered all over the walls, and you used to imagine walking in those distant lands.
  }
  \end{itemize}
}{}

\story{1}{The Special Item}
You may reveal you have any piece of \gls{adventure} equipment (see page \pageref{adventuringequipment}) or any item worth 10 \gls{sp} or less.
\iftoggle{verbose}{%
  Perhaps you have a flask of wine, kept from a special celebration, or a piece of chalk you kept after getting lost in a maze.

  \begin{itemize}
  \item The dungeon paths seem infinite, but you've come prepared.
  After getting trapped in the king's endless prison rooms, you've always carried a piece of chalk to mark your route when there are no other markings.
  \item The guards don't fancy letting some random wanderers through the door, but you've been saving a small bottle of dwarvish spirits for a special occasion, gifted by your family's dwarvish friend.
  With a bit of water, it should be strong enough for all four of them.
  \end{itemize}
}{}

\story{1}{Old Friend}
\label{sharedstories}%
At the point a new character joins the group you can select one other player and have a shared background with them (or with another, if your character is new).
You describe how you previously met and possibly travelled together.
From then on, you can split the cost of stories, so if the group wants to find a safe space to rest then instead of one character spending 2 Story points you could each spend 1.
Each of you can use characters from the other's background, because all your Stories have the option of being shared stories.
If you are both of noble heritage, any money you get must be divided between you.
If you are both friends with a skilled armourer, they will only be able to repair one piece of armour at a time.%
\footnote{This Story is transitive and symmetrical, so if player A shares a background with player B and player B shares a background with player C then player C also shares a background with player A.}

\iftoggle{verbose}{%
  \begin{itemize}
  \item
  When the game starts, you pick another character as a cousin.
  When you declare you know elvish, the other character does as well.
  Soon after, you both decide to return home, and get a royal welcome from the entire village.
  \item
  When a fellow \gls{pc} dies, someone needs to introduce a new character for them to return as.
  You spend 2 \glspl{storypoint}, as per \nameref{oldFriend}, and declare him your brother.
  \end{itemize}
}{}

\index{Mana Lakes}
\story{1}{The Mana Lake}
You know of a sacred location nearby, perhaps a church, or a shrine or just a sacred cavern where the land is teeming with magic.
In this sacred area, anyone stepping into it receives 1 \gls{mp} per \gls{round}.
If the spot has a guardian then they are friendly to you.
The place will not necessarily help you hide or defend yourself unless you are also spending \glspl{storypoint} to make it a place to rest.
\footnote{Those following the Code of Experience gain no \glsentrytext{xp} for finding this location.}

\iftoggle{verbose}{%
  \begin{itemize}
  \item After the battle, nobody has a drip of mana left, but you recall a nearby grove of elves, centred around a mana well.
  They helped you out last time you had to run away from the law, and you're hoping they'll help you again.
  \item You have the perfect idea for an artefact to help the town, but it will need a constant supply of mana.
  You remember the local temple has a mana font, and the local priest of war -- Lucretius -- owes you a favour from that time you defended the temple from a drunken rabble trying to steal weapons.
  \end{itemize}
}{}

\story{1}{The Old Friend}
\label{oldFriend}
You know someone in town who has just the skills you are all looking for.

The player can make this character themselves, just like a normal character, but cannot purchase Combat, Projectiles or \glspl{fp}.
The \gls{npc} begins with either 50 \glspl{xp}, or half the \glspl{xp} of whichever party member has the most (whichever is higher).
\iftoggle{verbose}{%
  So if one of the \glspl{pc} has 115 \glspl{xp}, the \gls{npc} would begin with 58 \glspl{xp}.
}{}

\iftoggle{verbose}{
  This is a particularly important story, as these form the secondary characters which players can use if their first characters die.

  \begin{itemize}
  \item Everyone wants to buy expensive chainmail, and your dwarvish friend just so happens to have retired here, selling top-quality armours of all types.
  \item The party needs an expert tracker, and on the road you meet your brother.
  He never liked people, so once he got out of the military, he began working independently as a bounty hunter.
  \end{itemize}
}{}

\story{2}{The Ally}

As per \nameref{oldFriend}, but the character can purchase any Trait.
These martial allies can accompany groups on dangerous missions.

\begin{itemize}
  \item
  With no idea how to talk to the local lord, you suddenly find your old military friend guarding the gate.
\end{itemize}

\story{2}{The Resting Spot}
You know of a secluded and secret location where you will be safe.
If your safe space is ever invaded due to events outside your control, you receive both Story points back if it is within the same session or 1 Story point back if it during a later session where the same place is used again.

\iftoggle{verbose}{%
  \begin{itemize}
  \item The guards may be chasing after you, but the Mincing Pig is nearby.  It's famous for some nasty customers and a deep cellar where even the town guard don't want to enter.
  It's been your regular bar since you were twelve years old, and you're sure they'll put you up.
  \item The goblins have found your tracks, and they'll catch up soon.
  However, you recall a nearby cave in the forest where you slept the last time you came through here on a mission with the other guards.
  \item The bandits are catching up soon, but you recall a walled village nearby.
  Your grandfather was the chief noble before he died, and you're hoping the guards remember you, despite your new beard.
  \end{itemize}
}{}

\iftoggle{aif}
{
  \story{1}{Guild Spy}
  You declare yourself to be a guild spy for a guild from another area, and immediately gain the rank of `associate'.%
  \footnote{See Fenestra's \autoref{guard}, page \pageref{ngAssociate}.}
  Of course your \glsentrytext{guard} duties will not end with this revelation -- in fact you must continue.

  Whatever your missions may be (and you must -- at the very least -- run it past the \gls{gm}), if it ever ends you will simply gain a new one from the guild, or just a message to `sit tight where you are'.

  Once your mission completes, your guild demands you remain for another (or goes silent\ldots).

  \begin{itemize}

    \item
    The Justice Guild of the Shale want to know more about why \glsentrytext{king} is sending so many of the \gls{guard} to your area, and not theirs.
    \item
    The Paper Guild wants to understand more about \gls{shatteredcastle}, and has sent a number of men out to listen quietly to what they might hear in the guard.
    \item
    Somewhere in the forest, the Final Guild know, rests a lost temple, cut off from civilization, but they will not tell you what lies there.

  \end{itemize}
}
{
  \story{3}{The Treasure}
  You have access to large funds now that you have returned to this area.
  The total amount obtained is $2D6 \times 10$ gold pieces.%
  \footnote{Those following the Code of Acquisition gain no \gls{xp} for gaining gold through \glspl{storypoint}.}

  \iftoggle{verbose}{%
    \begin{itemize}
    \item When you were back in the military, you and the platoon raided a rich person's house, stole a lot of items, then buried the valuables.
    You all swore not to return to the treasure for five years, but you need the money badly.
    \item The local noble is your father, and it's time to ask for a big favour; gold.
    While there, you spend 2 more \glspl{storypoint} to get a tracker, as you ask your father for a skilled warrior to help you track down the local bandits.
    \item You found the local noble was hoarding illegal magical items, and you agreed to keep quiet for money.
    It's time to ask for a little more hush money.
    \end{itemize}
  }{}
}

\end{multicols}

\iftoggle{aif}{
\section{Campaigns}

\begin{multicols}{2}

\noindent
Just because characters can begin as blank slates doesn't mean the story can.  A basic premise can help tie those backstories together.

If the \gls{gm} has no definitive plans laid out for the campaign, players should suggest a good starting point.

\subsection{The \Glsentrytext{guard}}

Adventuring may have been outlawed years ago, but you can still join the \gls{guard}!
This kind of campaign mean taking orders, getting missions, and rising through the ranks.

The party will have to decide how much looted wealth to report to their superiors, and which of their peers they can trust.

Think of the \gls{guard} as the default campaign within Fenestra, and take a look at Fenestra's \autoref{guard} for ideas on the various missions the \glspl{pc} can expect.

\subsection{The Illusionists of the Shale}

The characters are all gnomes, defending against encroaching goblins who have come through a portal, while the elders constantly argue that if only \emph{somehow} someone could get down there and destroy the portal, everyone would be safe.
But that ``somehow'' never comes, and the monsters are coming up faster and faster.
Rumours abound of distant elves who might help, but those elves have their own problems.
Meanwhile, the daily lives of the warrior-alchemists consists in setting and resetting various traps made of pitfalls, illusions and rope.  Each day the gnomes have to retreat farther from the deeps and closer to the Sun.

Once each member of the group has expended three \glspl{storypoint}, an opening comes to travel to the nearby elves, and beg for an army to save your homeland.

\subsection{The College of Alchemy}

The characters are all alchemists in the service of the \gls{college} in Eastlake.
The first part of the campaign involves high-school rivalries against other Clans in the guild such as stealing their homework, or vying for romantic attention.
Soon after, the characters begin proper guild missions, venturing out into the strange areas of the world where normal people will not tread.

At the campaign's start, characters get only 1 \gls{storypoint} each.
Each year's Summer holidays grants an additional \gls{storypoint}, so each character will have the full 5 \glspl{storypoint} at the end of the four year course.

\subsection{The Game Changer}

The party stumble across a game-changing magical item, capable of raining fire down on an entire battlefield.
They only need to find the activation word.

While the war is far away, both side know the party have the item, and want to take it from them.
Individual lords attempt to grab the item in order to take it to a king themselves, or even to start a rebellion against the king while the war is on.
Some know what the command word is, or at least claim to.

As the party moves the item to the battlefields, they hear more and more rumours about the activities of the kings on each side.
By the time they arrive, they will able to pick a side.

\end{multicols}

}{}
